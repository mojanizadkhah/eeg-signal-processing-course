%% EEG HW6
%% Q1 alef xn-xn+1 plot
clc;
clear;
a = 3.4;
y0 = 0.3;
n = 100;

x = 0:.01:1; 
y = a*x.*(1-x);
plot(x,y); 

hold on
plot(x,x);

hold on
[u,v,z] = iterate(a,y0,n);
plot(u,v);
xlabel("x(n)");
ylabel("x(n+1)");
title(["A is "+ num2str(a)+" & x0 is "+num2str(y0) ])
hold off

%% Q1 alef n-xn plot
n = 100;
a = 3.4;
y0 = 0.3;
i = 1:1:2*n-1;
[u,v,z] = iterate(a,y0,n);
plot(i,u);
hold on

a = 3.3;
y0 = 0.3;
i = 1:1:2*n-1;
[u,v,z] = iterate(a,y0,n);
plot(i,u);
xlabel("n");
ylabel("xn");
legend("a = 3.4" , "a = 3.3");
%% Q1 plots
figure()
subplot(2 , 1 , 1)
a = 0.3;
y0 = 0.3;
i = 1:1:2*n-1;
[u,v,z] = iterate(a,y0,n);
plot(i,u);
xlabel("n");
ylabel("xn");
title("a = 0.3")
subplot(2 , 1 , 2)
a = 0.7;
[u,v,z] = iterate(a,y0,n);
plot(i,u);
xlabel("n");
ylabel("xn");
title("a = 0.7")


figure()
subplot(2 , 1 , 1)
a = 1.3;
y0 = 0.3;
i = 1:1:2*n-1;
[u,v,z] = iterate(a,y0,n);
plot(i,u);
xlabel("n");
ylabel("xn");
title("a = 1.3")
subplot(2 , 1 , 2)
a = 1.7;
[u,v,z] = iterate(a,y0,n);
plot(i,u);
xlabel("n");
ylabel("xn");
title("a = 1.7")


figure()
subplot(2 , 1 , 1)
a = 2.3;
y0 = 0.3;
i = 1:1:2*n-1;
[u,v,z] = iterate(a,y0,n);
plot(i,u);
xlabel("n");
ylabel("xn");
title("a = 2.3")
subplot(2 , 1 , 2)
a = 2.7;
[u,v,z] = iterate(a,y0,n);
plot(i,u);
xlabel("n");
ylabel("xn");
title("a = 2.7")


figure()
subplot(2 , 1 , 1)
a = 3.3;
y0 = 0.3;
i = 1:1:2*n-1;
[u,v,z] = iterate(a,y0,n);
plot(i,u);
xlabel("n");
ylabel("xn");
title("a = 3.3")
subplot(2 , 1 , 2)
a = 3.7;
[u,v,z] = iterate(a,y0,n);
plot(i,u);
xlabel("n");
ylabel("xn");
title("a = 3.7")

%% Q1 be bifurcation
clf
hold
for m=1:1000
    a = m*0.004;
    z = itte(a,0.35,1000);
    plot(a, z(950:1000),".")
end
%% Q2 alef (duffing -plotting time series-)
global r s af w
r = 0.3;
s = 1;
w=1.2;
af=1.5;

initials = [1 0];
[t x]=ode45(@duffingfunc,0:2*pi/(af*200):4000,initials);

figure(1)
plot(t(1:2000),x(1:2000,1),'r')
xlabel("t");
ylabel("")
title('time series(af = 1.5)')
i = 0;
figure(2)
for af = [0.2 0.28 0.29 0.37 0.5 0.65]
    i = i+1;
    [t x]=ode45(@duffingfunc,0:2*pi/(af*200):4000,initials);

    subplot(3 , 2 , i)
    plot(t(1000:4000),x(1000:4000,1));
    xlabel("t");
    ylabel(["Af is "+ num2str(af)])
    title('time series')

end
%% Q2 be(phase space)
i = 0;
figure(2)
for af = [0.2 0.28 0.29 0.37 0.5 0.65]
    i = i+1;
    [t x]=ode45(@duffingfunc,0:2*pi/(af*200):4000,initials);

    subplot(3 , 2 , i)
    plot(x(1000:10000,2),x(1000:10000,1));
    xlabel("t");
    ylabel(["Af is "+ num2str(af)])
    title('time series')

end
%% Q3
clc;
clear;
fs = 100;
t = 1/fs:1/fs:50;
load anesthesia.mat;
awakke = zeros(16 , 5*fs);
deeep = zeros(16 , 5*fs);
for i = 0:15
    awakke(i+1 , :) = awake(1+300*i: 500+300*i,1 );
    deeep(i+1 , :) = deep(1+300*i: 500+300*i,1 );   
end
%% Q3 alef
SE = shannon_entropy(awake');
%% Q3 be and pe
shannonawake = zeros(1 , 16);
shannondeep = zeros(1 , 16);
for i = 1:16
    shannonawake(1 , i) = shannon_entropy(awakke(i , :));
    shannondeep(1 , i) = shannon_entropy(deeep(i , :));
end
shannonent = [shannonawake , shannondeep];

i = 1:1:17;
plot(i , shannonent(1:17),'b');
hold on
i = 17:1:32;
plot(i , shannonent(17:32) , 'r');
hold on
i = 1:1:17;
scatter(i , shannonent(1:17),'b' ,'filled');
hold on
i = 17:1:32;
scatter(i , shannonent(17:32) , 'r' ,'filled');
xlabel("t");
title("first awake then sleep for Shannon Entropy");
%% Q3 te
clc;
m = 2;
rr = 0.5;
AEawake = zeros(1 , 16);
AEdeep = zeros(1 , 16);
for i = 1:16
    AEawake(1 , i) = approximate_entropy(awakke(i , :),m,rr);
    AEdeep(1 , i) = approximate_entropy(deeep(i , :),m,rr);
end
AEtot = [AEawake , AEdeep];

i = 1:1:17;
plot(i , AEtot(1:17),'b');
hold on
i = 17:1:32;
plot(i , AEtot(17:32) , 'r');
hold on
i = 1:1:17;
scatter(i , AEtot(1:17),'b' ,'filled');
hold on
i = 17:1:32;
scatter(i , AEtot(17:32) , 'r' ,'filled');
xlabel("t");
title("first awake then sleep for Approximate Entropy");
%% Q3 se

m = 3;
t = 2;
PEawake = zeros(1 , 16);
PEdeep = zeros(1 , 16);
for i = 1:16
    PEawake(1 , i) = permutation_entropy(awakke(i , :),m,t);
    PEdeep(1 , i) = permutation_entropy(deeep(i , :),m,t);
end
PEtot = [PEawake , PEdeep];

i = 1:1:17;
plot(i , PEtot(1:17),'r');
hold on
i = 17:1:32;
plot(i , PEtot(17:32) , 'b');
xlabel("t");
hold on
i = 1:1:17;
scatter(i , PEtot(1:17),'r' , 'filled');
hold on
i = 17:1:32;
scatter(i , PEtot(17:32) , 'b' ,  'filled');
title("first awake then sleep for Permutation Entropy");
%% Q4
clc;
clear;
a = 3.4;
y0 = 0.3;
n = 1000;
[u,v,z] = iterate(a,y0,n);
% [x , y] = CorrelationDimention(u , 1000);
% plot(x , y)
% % ymax = max(y);
% % ymin = min(y(end/2:end));
% 
% % p = polyfit(x,y,10);
a = [0.05:0.05:4];
y = zeros(80 , 1);
for i = 1:80
    y0 = 0.3;
    n = 100;
    [u,v,z] = iterate(a(i),y0,n);
    y(i) = corrdim(u);
end

plot(a(1:80) , y(1:80));
xlabel("a");
title("corelation dimention");
%% functions

function [u, v, z] = iterate(a, y0, n)
    z = zeros(n+1,1);
    z(1) = y0;
    for i = 2:n
    z(i) = a*z(i-1).*(1-z(i-1));
    end
    u = zeros(2*n,1);
    u(1) = y0;
    v = zeros(2*n,1);
    v(1) = 0;

    for i=2:2*n
    u(i) = z(ceil(i/2));
    end
    v = [0;u(3:2*n)];
    u = u(1:2*n-1);
end

function [z] = itte(a, y0, n)
    z = zeros(n+1,1);
    z(1) = y0;
    for i = 2:n
        z(i) = a*z(i-1).*(1-z(i-1));
    end
end

function xdot=duffingfunc(t,x)
    global r s af w
    xdot(2)=x(1);
    xdot(1)=  -r*x(1)  +  x(2)  -s*x(2)^3  +  af*cos(w*t);
    xdot=xdot';
end

function SN = shannon_entropy(signal)
    h1=histogram(signal, 1000);
    h1.Normalization = 'probability';
    p = h1.Values();

    summ = 0;
    for i = 1:length(p)
        if (p(i))
            summ =summ + (p(i).*log2(p(i)));
        end
    end
    SN = -summ;
    
%     h1=histogram(signal, 'Normalization', 'Probability');
%     h1.Values;
%     entropy = -sum(p.*log2(p));
end

function [apen] = approximate_entropy(data,m,r)
    t = m;
    l = 1;
    for m=t:t+1
        N = length(data(1 , :));
        len = N - m +1;
        u = zeros(len , m);
        tt = 0;
        counter = zeros(len);
        for i=1:len
            u = data(i:i+m-1);

            for j=1:len
                s = data(j:j+m-1); 

                q = max(abs(u-s));
                flag = (q>r); 
                if(flag==0) 
                     tt = tt+1;
                end
            end
           counter(i)=tt/len;
           tt=0;
        end 
        if(l==1)
            corr1 = log(sum(sum(counter)))/len;
        else
            corr2 = log(sum(sum(counter)))/len;
        end
        l = l+1;

    end  
    apen = (corr1-corr2);

end
function out = corrdim(data)
    MinR = 0.05;
    MaxR = 2;
    Np = 100;
    out = correlationDimension(data,'MinRadius',MinR,'MaxRadius',MaxR,'NumPoints',Np);
end

function [x , y] = CorrelationDimention(data , rr)
a = 3.4;
y0 = 0.3;
n = 200;
[u,v,z] = iterate(a,y0,n);

rr = 30;
data = u';


    C = zeros(1 , rr);
    x = zeros(1 , rr);
    y = zeros(1 , rr);
    len = length(data(1 , :));
    phi = zeros(len , len);
    for r = 1:1:rr
        for i =1:len
            for j = 1:len
                if(j~=i)
                    d = 10^(-r);
                    if( d >= abs(data(1 , i)- data(1 , j)) )
                        phi(i , j)=1/(len*(len-1));
%                         phi(i , j)=1;
                    end
                end
            end
        end
        C(r)= sum(sum(phi));
        y(1 , r) = (C(r));
        x(1 , r) = (1/r);
    end


end