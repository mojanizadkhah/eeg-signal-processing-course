%% HW7_EEG_matlab
%% alef
clc;
clear;

load('ElecPosXYZ');

%Forward Matrix
ModelParams.R = [8 8.5 9.2] ;
ModelParams.Sigma = [3.3e-3 8.25e-5 3.3e-3];
ModelParams.Lambda = [.5979 .2037 .0237];
ModelParams.Mu = [.6342 .9364 1.0362];

Resolution = 1 ;
[LocMat,GainMat] = ForwardModel_3shell(Resolution, ModelParams) ;

scatter3(LocMat(1 , :) , LocMat(2 , :) , LocMat(3 , :) ,'.' );
xlabel("x axis");
ylabel("y axis");
zlabel("z axis");
title("position of bipoles");
 % GainMat is matris "bahre"!
%% be
radius = 9.2;
pos = zeros(21 , 3);
names = string(21);
load ElecPosXYZ.mat
for i= 1:21
    pos(i , :) = ElecPos{1, i}.XYZ;
    names(1 , i) = ElecPos{1, i}.Name;
end
pos = radius*pos';


scatter3(LocMat(1 , :) , LocMat(2 , :) , LocMat(3 , :) ,'.'  );
xlabel("x axis");
ylabel("y axis");
zlabel("z axis");
title("position of bipoles");
hold on

scatter3(pos(1 , :) , pos(2 , :) , pos(3 , :) , 'filled' );
for i = 1 : 21
    text(pos(1 , i), pos(2 , i),pos(3 , i), names(i));
    hold on
end


%% pe
% %  my first approach
% radius = 9.2;
% phi = (pi)*rand()/2;
% theta = (2*pi)*rand() - pi;
% x = radius*sin(phi)*cos(theta);
% y = radius*sin(phi)*sin(theta);
% z = radius*cos(phi);
% bii = cat(1 , x , y , z);


num = randi(length(LocMat));
x=LocMat(1,num);
y=LocMat(2,num);
z=LocMat(3,num);

scatter3(LocMat(1 , :) , LocMat(2 , :) , LocMat(3 , :) ,'.'  );
xlabel("x axis");
ylabel("y axis");
zlabel("z axis");
title("position of bipoles");
hold on

scatter3(pos(1 , :) , pos(2 , :) , pos(3 , :) , 'filled' );
for i = 1 : 21
    text(pos(1 , i), pos(2 , i),pos(3 , i), names(i));
    hold on
end

hold on
scatter3(x , y , z ,'filled' );
eq = [x ; y ; z]./sqrt(x^2 + y^2 + z^2);
for r=8.5:1/200:9.2
    x=r*eq(1);
    y=r*eq(2);
    z=r*eq(3);
    plot3(x,y,z,'.');
end
legend("position of bipoles","position of electrodes" , "random bipol");
%% te
fs = 100;

n = length(LocMat);
load Interictal.mat;

q = Interictal(5 , :);

Q = eq*q;
G = GainMat(: , 3*(num-1)+1:3*num);
M = G*Q;

disp_eeg(M,max(max(M)),100,names,"M (part te)");
%% se

t = [10.89*100 17.39*100 35.62*100 83.06*100 91.01*100 93.57*100];
len = length(Interictal(1 , :));
w = zeros(1 , len);
mmean = zeros(1 , 21);
mmean1 = zeros(1 , 21);
for j = 1:length(t)
    w(t(j)-3:t(j)+3) = 1;  
    for i = 1:21
        mmean(i) = mean(M(i , t(j)-3:t(j)+3));
    end
end


Nmean = normalize(mmean);

figure();
scatter3(pos(1 , :) , pos(2 , :) , pos(3 , :) ,[],Nmean  , 'filled' );
colormap(jet);
hold on

for i = 1 : 21
    text(pos(1 , i), pos(2 , i),pos(3 , i),  names(i));
    hold on
end
%% je
figure();
Display_Potential_3D(9.2,Nmean);
%% che
clear Qmne & Qwmne & Qlor;
clc;
% MNE
G = GainMat;
N = 21;
alfa = 1.2;
% t = 1:1:len
Qmne = G'*(G*G' + alfa*eye(N))^-1*M;
%% WMNE

omega=zeros(1317);
for i=1:length(omega(1 , :))
    t=0;
    for k=1:21
        t = t + GainMat(k,3*i-2:3*i)*GainMat(k,3*i-2:3*i)';
    end
    omega(i,i) = sqrt(t);
end

W = kron(omega,eye(3)); 
clear Qwmne
Qwmne = (W'*W)^-1*G'*(G*(W'*W)^-1*G' + alfa*eye(N))^-1*M;

%% LORETA
d = 1;
P = 1317;
A1 = zeros(P,P);
for i = 1:P
    for j = 1:P
        distance = norm(LocMat(:,i)-LocMat(:,j));
        if distance == d
            A1(i,j) = 1/6;
        end
    end
end

A0 = ((A1*ones(P)) .* eye(P))^-1 * A1;
B = 6/d^2* (kron(A0, eye(3)) - eye(3*P));
W = kron(omega, eye(3)) * B' * B * kron(omega, eye(3));

Qlor = (W'*W)^-1*G'*(G*(W'*W)^-1*G' + alfa*eye(N))^-1*M;

%% he mne
temp = zeros(3951/3 , 10240);
for i = 1:(10240)
    for j = 1:(3951/3)
        temp(j , i) = sqrt(Qmne(3*j-2 , i)^2 + Qmne(3*j-1 , i)^2 + Qmne(3*j , i)^2);
    end
end

maxx = max(max(temp'));
indmne = find((max(temp'))== maxx);
eqmne=[LocMat(1,indmne),LocMat(2,indmne),LocMat(3,indmne)]/sqrt(LocMat(1,indmne)^2 + LocMat(2,indmne)^2 + LocMat(3,indmne)^2);

temp = zeros(3951/3 , 10240);
for i = 1:(10240)
    for j = 1:(3951/3)
        temp(j , i) = sqrt(Qwmne(3*j-2 , i)^2 + Qwmne(3*j-1 , i)^2 + Qwmne(3*j , i)^2);

    end
end
maxx = max(max(temp'));
indwmne = find((max(temp'))== maxx);
eqwmne=[LocMat(1,indwmne),LocMat(2,indwmne),LocMat(3,indwmne)]/sqrt(LocMat(1,indwmne)^2 + LocMat(2,indwmne)^2 + LocMat(3,indwmne)^2);

temp = zeros(3951/3 , 10240);
for i = 1:(10240)
    for j = 1:(3951/3)
        temp(j , i) = sqrt(Qlor(3*j-2 , i)^2 + Qlor(3*j-1 , i)^2 + Qlor(3*j , i)^2);

    end
end
maxx = max(max(temp'));
indlor = find((max(temp'))== maxx);
eqlor=[LocMat(1,indlor),LocMat(2,indlor),LocMat(3,indlor)]/sqrt(LocMat(1,indlor)^2 + LocMat(2,indlor)^2 + LocMat(3,indlor)^2);

%% khe
Emne = sqrt(sum((LocMat(: , indmne) - [x; y; z]).^2));
Ewmne = sqrt(sum((LocMat(: , indwmne) - [x; y; z]).^2));
Elor = sqrt(sum((LocMat(: , indlor) - [x; y; z]).^2));
%%  de
% pe
num = randi(length(LocMat));
x=LocMat(1,num);
y=LocMat(2,num);
z=LocMat(3,num);
ro = sqrt(sum(x*x + y*y + z*z));
% theta = arctan(x/y)
% phi = arctan(norm(x,y)/z);

scatter3(LocMat(1 , :) , LocMat(2 , :) , LocMat(3 , :) ,'.'  );
xlabel("x axis");
ylabel("y axis");
zlabel("z axis");
title("position of bipoles");
hold on

scatter3(pos(1 , :) , pos(2 , :) , pos(3 , :) , 'filled' );
for i = 1 : 21
    text(pos(1 , i), pos(2 , i),pos(3 , i), names(i));
    hold on
end

hold on
scatter3(x , y , z ,'filled' );
legend("position of bipoles","position of electrodes" , "random bipol");
%% te
fs = 100;
Resolution = 1;
[LocMat,GainMat] = ForwardModel_3shell(Resolution, ModelParams) ;
n = length(LocMat);
load Interictal.mat;

q = Interictal(5 , :);
eq = [x ; y ; z]./sqrt(x^2 + y^2 + z^2);
Q = eq*q;
G = GainMat(: , 3*(num-1)+1:3*num);
M = G*Q;

disp_eeg(M,max(max(M)),100,names,"M (part de)");
%% se

t = [10.89*100 17.39*100 35.62*100 83.06*100 91.01*100 93.57*100];
len = length(Interictal(1 , :));
w = zeros(1 , len);
mmean = zeros(1 , 21);
mmean1 = zeros(1 , 21);
for j = 1:length(t)
    w(t(j)-3:t(j)+3) = 1;  
    for i = 1:21
        mmean(i) = mean(M(i , t(j)-3:t(j)+3));
    end
end


Nmean = normalize(mmean);

figure();
scatter3(pos(1 , :) , pos(2 , :) , pos(3 , :) ,[],Nmean  , 'filled' );
colormap(jet);
hold on

for i = 1 : 21
    text(pos(1 , i), pos(2 , i),pos(3 , i),  names(i));
    hold on
end
%% je
figure()
a = sqrt(sum(x^2+y^2+z^2));
Display_Potential_3D(a,Nmean);
%%
% MNE
G = GainMat;
N = 21;
alfa = 2.5;
Qmne = G'*(G*G' + alfa*eye(N))^-1*M;
% WMNE

omega=zeros(1317,1317);
for i=1:length(omega(1 , :))
    t=0;
    for k=1:21
        t = t + GainMat(k,3*i-2:3*i)*GainMat(k,3*i-2:3*i)';
    end
    omega(i,i) = sqrt(t);
end

W = kron(omega,eye(3)); 
Qwmne = (W'*W)^-1*G'*(G*(W'*W)^-1*G' + alfa*eye(N))^-1*M;

% LORETA
d = 1;
P = 1317;
A1 = zeros(P,P);
for i = 1:P
    for j = 1:P
        distance = norm(LocMat(:,i)-LocMat(:,j));
        if distance == d
            A1(i,j) = 1/6;
        end
    end
end

A0 = ((A1*ones(P)) .* eye(P))^-1 * A1;
B = 6/d^2* (kron(A0, eye(3)) - eye(3*P));
W = kron(omega, eye(3)) * B' * B * kron(omega, eye(3));

Qlor = (W'*W)^-1*G'*(G*(W'*W)^-1*G' + alfa*eye(N))^-1*M;

% he mne
temp = zeros(3951/3 , 10240);
for i = 1:(10240)
    for j = 1:(3951/3)
        temp(j , i) = sqrt(Qmne(3*j-2 , i)^2 + Qmne(3*j-1 , i)^2 + Qmne(3*j , i)^2);
    end
end

maxx = max(max(temp'));
indmne = find((max(temp'))== maxx);
eqmne=[LocMat(1,indmne),LocMat(2,indmne),LocMat(3,indmne)]/sqrt(LocMat(1,indmne)^2 + LocMat(2,indmne)^2 + LocMat(3,indmne)^2);

temp = zeros(3951/3 , 10240);
for i = 1:(10240)
    for j = 1:(3951/3)
        temp(j , i) = sqrt(Qwmne(3*j-2 , i)^2 + Qwmne(3*j-1 , i)^2 + Qwmne(3*j , i)^2);

    end
end
maxx = max(max(temp'));
indwmne = find((max(temp'))== maxx);
eqwmne=[LocMat(1,indwmne),LocMat(2,indwmne),LocMat(3,indwmne)]/sqrt(LocMat(1,indwmne)^2 + LocMat(2,indwmne)^2 + LocMat(3,indwmne)^2);

temp = zeros(3951/3 , 10240);
for i = 1:(10240)
    for j = 1:(3951/3)
        temp(j , i) = sqrt(Qlor(3*j-2 , i)^2 + Qlor(3*j-1 , i)^2 + Qlor(3*j , i)^2);

    end
end
maxx = max(max(temp'));
indlor = find((max(temp'))== maxx);
eqlor=[LocMat(1,indlor),LocMat(2,indlor),LocMat(3,indlor)]/sqrt(LocMat(1,indlor)^2 + LocMat(2,indlor)^2 + LocMat(3,indlor)^2);

% khe
Emne = sqrt(sum((LocMat(: , indmne) - [x; y; z]).^2));
Ewmne = sqrt(sum((LocMat(: , indwmne) - [x; y; z]).^2));
Elor = sqrt(sum((LocMat(: , indlor) - [x; y; z]).^2));
%% ze
% %Forward Matrix
ModelParams.R = [9.2] ;
ModelParams.Sigma = [3.3e-3 8.25e-5 3.3e-3];
ModelParams.Lambda = [.5979 .2037 .0237];
ModelParams.Mu = [.6342 .9364 1.0362];
Resolution = 1 ;
[LocMat,GainMat] = ForwardModel_3shell(Resolution, ModelParams) ;
L = LocMat;

% for i = 1:2000
%     radius = 9.2;
%     phi = (pi)*rand()/2;
%     theta = (2*pi)*rand() - pi;
%     L(1 , i) = radius*sin(phi)*cos(theta);
%     L(2 , i) = radius*sin(phi)*sin(theta);
%     L(3 , i) = radius*cos(phi);
% end


ll = length(L(1 , :));
disst = zeros(ll, ll);
for i = 1:ll
    for j = 1:ll
        disst(i , j) = norm(L(: , i) - L(: , j));
    end
end
%%
rr = randi(ll);
[A,I1] = sort(disst(rr , :));
% D = disst(rr , I);
good = L(: , I1(1:18));
lenn = length(good(1 , :));
eq = zeros(3 , lenn);
for i = 1:lenn
    eq(1 , i)= good(1 ,i)/norm(good(: ,i));
    eq(2 , i)= good(2 ,i)/norm(good(: ,i));
    eq(3 , i)= good(3 ,i)/norm(good(: ,i));
end
scatter3(L(1 , :) , L(2 , :) , L(3 , :) ,'.'  );
xlabel("x axis");
ylabel("y axis");
zlabel("z axis");
title("position of bipoles");
hold on

scatter3(pos(1 , :) , pos(2 , :) , pos(3 , :) , 'filled' );
for i = 1 : 21
    text(pos(1 , i), pos(2 , i),pos(3 , i), names(i));
    hold on
end

for i = 1:lenn
    scatter3(good(1 , i) , good(2 , i) , good(3 , i) ,'filled' );
    hold on
    for r=8.5:1/200:9.2
        x=r*eq(1,i);
        y=r*eq(2,i);
        z=r*eq(3,i);
        plot3(x,y,z,'.');
    end
end

% phithe = zeros(2 , lenn); % first row theta, second row phi 
% for i = 1:18
%     m = good(1 , i)/good(2 , i)
%     phithe(1 , i) = arctan(m);
%     phithe(1 , i) = arctan(norm(good(1 , i),good(2 , i))/good(3 , i));
% end

%% re
fs = 100;
Resolution = 1;
[LocMat,GainMat] = ForwardModel_3shell(Resolution, ModelParams) ;
n = length(LocMat);

q = repmat(Interictal(5 , :) ,[lenn 1] );
% Q = zeros(lenn , 10240);
for i = 1:lenn
    x = good(1 , i);
    y = good(2 , i);
    z = good(3 , i);
    Q(3*i-2 , :) = q(i , :)*(x/sqrt(x^2 + y^2 + z^2));
    Q(3*i-1 , :) = q(i , :)*(y/sqrt(x^2 + y^2 + z^2));
    Q(3*i   , :) = q(i , :)*(z/sqrt(x^2 + y^2 + z^2));
end


G = GainMat(: , end-3*lenn+1:end);
M = G*Q;

disp_eeg(M,max(max(M)),100,names,"M (part re)");
%%
t = [10.89*100 17.39*100 35.62*100 83.06*100 91.01*100 93.57*100];
len = length(Interictal(1 , :));
w = zeros(1 , len);
mmean = zeros(1 , 21);
mmean1 = zeros(1 , 21);
for j = 1:length(t)
    w(t(j)-3:t(j)+3) = 1;  
    for i = 1:21
        mmean(i) = mean(M(i , t(j)-3:t(j)+3));
    end
end
Nmean = normalize(mmean);

figure();
scatter3(pos(1 , :) , pos(2 , :) , pos(3 , :) ,[],Nmean  , 'filled' );
colormap(jet);
hold on

for i = 1 : 21
    text(pos(1 , i), pos(2 , i),pos(3 , i),  names(i));
    hold on
end
figure()
Display_Potential_3D(9.2,Nmean);

%% Re last part(like che)
% MNE
G = GainMat;
N = 21;
alfa = 2.2;
Qmne = G'*(G*G' + alfa*eye(N))^-1*M;

% WMNE
% omega=zeros(1317,1317);
omega=zeros(1993,1993);
for i=1:length(omega(1 , :))
    t=0;
    for k=1:21
        t = t + GainMat(k,3*i-2:3*i)*GainMat(k,3*i-2:3*i)';
    end
    omega(i,i) = sqrt(t);
end

W = kron(omega,eye(3)); 
Qwmne = (W'*W)^-1*G'*(G*(W'*W)^-1*G' + alfa*eye(N))^-1*M;

% LORETA
d = 1;
% P = 1317;
P = 1993;
A1 = zeros(P,P);
for i = 1:P
    for j = 1:P
        distance = norm(LocMat(:,i)-LocMat(:,j));
        if distance == d
            A1(i,j) = 1/6;
        end
    end
end

A0 = ((A1*ones(P)) .* eye(P))^-1 * A1;
B = 6/d^2* (kron(A0, eye(3)) - eye(3*P));
W = kron(omega, eye(3)) * B' * B * kron(omega, eye(3));

Qlor = (W'*W)^-1*G'*(G*(W'*W)^-1*G' + alfa*eye(N))^-1*M;

%  mne
temp = zeros(1993 , 10240);
for i = 1:(10240)
    for j = 1:(3951/3)
        temp(j , i) = sqrt(Qmne(3*j-2 , i)^2 + Qmne(3*j-1 , i)^2 + Qmne(3*j , i)^2);
    end
end
tempp1 = max(temp');
[maxx ,I] = sort(tempp1);
eqmne = zeros(3 , lenn);
indmne = I(end-lenn+1:end);

for i = 1:18
    eqmne(: , i)=[LocMat(1,indmne(i)),LocMat(2,indmne(i)),LocMat(3,indmne(i))]/sqrt(LocMat(1,indmne(i))^2 + LocMat(2,indmne(i))^2 + LocMat(3,indmne(i))^2);
end

% Wmne
temp = zeros(1993 , 10240);
for i = 1:(10240)
    for j = 1:(3951/3)
        temp(j , i) = sqrt(Qwmne(3*j-2 , i)^2 + Qwmne(3*j-1 , i)^2 + Qwmne(3*j , i)^2);
    end
end
tempp2 = max(temp');
[maxx ,I] = sort(tempp2);
eqwmne = zeros(3 , lenn);
indwmne = I(end-lenn+1:end);

for i = 1:18
    eqwmne(: , i)=[LocMat(1,indwmne(i)),LocMat(2,indwmne(i)),LocMat(3,indwmne(i))]/sqrt(LocMat(1,indwmne(i))^2 + LocMat(2,indwmne(i))^2 + LocMat(3,indwmne(i))^2);
end
% loreta
temp = zeros(1993 , 10240);
for i = 1:(10240)
    for j = 1:(3951/3)
        temp(j , i) = sqrt(Qlor(3*j-2 , i)^2 + Qlor(3*j-1 , i)^2 + Qlor(3*j , i)^2);
    end
end
tempp3 = max(temp');
[maxx ,I] = sort(tempp3);
eqlor = zeros(3 , lenn);
indlor = I(end-lenn+1:end);

for i = 1:18
    eqlor(: , i)=[LocMat(1,indlor(i)),LocMat(2,indlor(i)),LocMat(3,indlor(i))]/sqrt(LocMat(1,indlor(i))^2 + LocMat(2,indlor(i))^2 + LocMat(3,indlor(i))^2);
end
% khe
Emne = sum(sqrt(sum((LocMat(: , indmne) - good).^2)))/lenn;
Ewmne = sum(sqrt(sum((LocMat(: , indwmne) - good).^2)))/lenn;
Elor = sum(sqrt(sum((LocMat(: , indlor) - good).^2)))/lenn;
%%

% 1993
label = zeros(1 , 1993);
for i = 1:1993
    if(ismember(i , I1(1:18)))
        label(i)=1;
    end
end
%%
[X1,Y1,T1,AUC] = perfcurve(label,tempp1,1);
figure();
plot(X1 , Y1);
title("ROC MNE");
%%
[X2,Y2,T2,AUC] = perfcurve(label,tempp2,1);
figure();
plot(X2 , Y2);
title("ROC WMNE");
%%
[X3,Y3,T3,AUC] = perfcurve(label,tempp3,1);
figure();
plot(X3 , Y3);
title("ROC LORETA");
