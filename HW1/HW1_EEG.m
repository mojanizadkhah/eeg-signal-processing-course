%% Q1 alef
clc;
clear;
t = 0:1/1e3:2;
f = chirp(t,100,1,500,'quadratic');
x = f;

figure();
plot(t , f)
xlabel("t(s)");
ylabel("f(t)");


%% Q1 be
wvtool(hamming(128),rectwin(128),gausswin(128) , triang(128))

wvtool(hamming(128))
wvtool(rectwin(128))
wvtool(gausswin(128))
wvtool(triang(128))


%% Q1 pe 
clc;
L = 128;
nfft = L;
noverlap = 0;
window = 128;
fs = 1000;
t = 0:1/fs:2;
x = chirp(t,100,1,500,'quadratic');
figure(1)
spectrogram(x,hamming(L),noverlap,nfft , fs , 'yaxis');
title("stft (Hamming window)");
figure(2)
spectrogram(x,rectwin(L),noverlap,nfft, fs , 'yaxis');
title("stft (Rectangle window)");
figure(3)
spectrogram(x,gausswin(L),noverlap,nfft, fs , 'yaxis');
title("stft (Gaussian window)");
figure(4)
spectrogram(x,triang(L),noverlap,nfft, fs , 'yaxis');
title("stft (Triangle window)");


%% Q1 te 
noverlap = 0;
figure(1);
spectrogram(x,rectwin(L),noverlap,nfft,fs , 'yaxis');
title("stft Rectangle (Noverlap 0)");

noverlap = 64;
figure(2);
spectrogram(x,rectwin(L),noverlap,nfft,fs , 'yaxis');
title("stft Rectangle (Noverlap 64)");

noverlap = 127;
figure(3);
spectrogram(x,rectwin(L),noverlap,nfft,fs , 'yaxis');
title("stft Rectangle (Noverlap 127)");


%% Q1 se
L = 32;
nfft = L;
noverlap = L-1;
figure(1);
spectrogram(x,hamming(L),noverlap,nfft,fs , 'yaxis');
title("stft Hamming (L=32)");

L = 128;
noverlap = L-1;
nfft = L;
figure(2);
spectrogram(x,hamming(L),noverlap,nfft,fs , 'yaxis');
title("stft Hamming (L=128)");

L = 512;
noverlap = L-1;
nfft = L;
figure(3);
spectrogram(x,hamming(L),noverlap,nfft,fs , 'yaxis');
title("stft Hamming (L=512)");


%% Q1 je

L = 128;
noverlap = L/2;

nfft = L;
figure();
spectrogram(x,gausswin(L),noverlap,nfft,fs , 'yaxis');
title("stft Gaussian (nfft = L = 128)");

nfft = 2*L;
figure();
spectrogram(x,gausswin(L),noverlap,nfft,fs , 'yaxis');
title("stft Gaussian (nfft = 2*L = 256)");

nfft = 4*L;
figure();
spectrogram(x,gausswin(L),noverlap,nfft,fs , 'yaxis');
title("stft Gaussian (nfft = 4*L = 512)");


%% Q1 de
L = 128;
noverlap = L/2;
nfft = L;
figure();
[stft, f, t]  = outstft(x,gausswin(L),noverlap,nfft,fs);
[stftm, fm, tm]  = spectrogram(x,gausswin(L),noverlap,nfft,fs);

f = f(:);
errorf = sum(f(1:65)-fm(1:65))/65;
errort = sum(t(1:30)-tm(1:30))/30;
errorstft = sum(sum(  (stft(1:65 , :)-stftm(1:65 , :)).^2  ))/(65*30);

L = 32;
noverlap = 0;
nfft = L;
[stft, f, t]  = outstft(x,rectwin(L),noverlap,nfft,fs);
[stftm, fm, tm]  = spectrogram(x,rectwin(L),noverlap,nfft,fs);


%% Q2 alef
clc;
clear;
load("NewEEGSignal");
signal = NewEEGSignal;
len = 512;
fs = 256;
T = 1/fs;             % Sampling period       
L = 512;             % Length of signal
t = (0:L-1)*T; 
f = fs*((-L+1)/2:(L-1)/2)/L;

figure();
t = 1/fs:1/fs:2;
plot(t , signal);
xlabel("t(s)");
ylabel("v(mv)");
title("EEG in time domain");

figure();
stft(signal , fs);

figure();
Y = fftshift(fft(signal));
plot(f,Y)
xlim([-100  , 100]);
xlabel("frequency(Hz)");
% ylabel("V(mv)");
title("signal in freq domain");
%%
N = numel(signal);
t = (0:L-1)/fs; 
subplot(2,1,1)
plot(1e3*t,signal)
xlabel('Time (ms)')
ylabel('Amplitude')
title('EEG Signal')
subplot(2,1,2)
pspectrum(signal,fs,'Leakage',1,'FrequencyLimits',[0, 128]);


%%  Q2 be

[b,a] = butter(5,60/(fs/2));
filt_signal = filter(b,a,signal);

filt_Y = fftshift(fft(filt_signal));
plot(f,filt_Y)

xlim([-75  , 75]);
xlabel("frequency(Hz)");
ylabel("V(mv)");
title("EEG in frequency domain after LPF butterworth filter (60Hz)");

rate = 2;
EEG_ds=downsample(filt_signal,rate);


%% plotting the EEG_ds

T = rate/fs;             % Sampling period       
L = 512/rate;             % Length of signal
t = (0:L-1)*T; 
f0 = fs*((-L+1)/2:(L-1)/2)/L;

figure();
plot(t , EEG_ds);
xlabel("t(s)");
ylabel("v(mv)");
title("EEG ds in time domain");

figure()
stft(EEG_ds , fs);

figure();
sig0 = fftshift(fft(EEG_ds));
plot(f0,sig0)
xlim([-100  , 100]);
xlabel("frequency(Hz)");
ylabel("V(mv)");
title("EEG ds in freq domain");


%% Q2 pe
[a , N] = size(EEG_ds);
fEEG_ds = abs(fftshift(fft(EEG_ds , N)));

EEG_ds1 = zeros(1 , N/2);
EEG_ds1(1 , :) = EEG_ds(1 , 1:N/2);
fEEG_ds1 = abs(fftshift(fft(EEG_ds1 , N/2)));

EEG_ds2 = zeros(1 , N/4);
EEG_ds2(1 , :) = EEG_ds(1 , 1:N/4);
fEEG_ds2 = abs(fftshift(fft(EEG_ds2 , N/4)));

EEG_ds3 = zeros(1 , N/8);
EEG_ds3(1 , :) = EEG_ds(1 , 1:N/8);
fEEG_ds3 = abs(fftshift(fft(EEG_ds3 , N/8)));

figure();
subplot(5 , 1 , 1);
plot(f , abs(Y));
xlim([-70  , 70]);
ylabel("512 point DFT");
xlabel("f(Hz)");

subplot(5 , 1 , 2);
f0 = fs*((-L+1)/2:(L-1)/2)/L;
plot(f0 , fEEG_ds);
xlim([-70  , 70]);
ylabel("256 point DFT");
xlabel("f(Hz)");

subplot(5 , 1 , 3);
n1 = 256/2;
f1 = fs*((-n1+1)/2:(n1-1)/2)/n1;
plot(f1 ,fEEG_ds1);
xlim([-70  , 70]);
ylabel("128 point DFT");
xlabel("f(Hz)");

subplot(5 , 1 , 4);
n1 = 256/4;
f1 = fs*((-n1+1)/2:(n1-1)/2)/n1;
plot(f1 ,fEEG_ds2);
xlim([-70  , 70]);
ylabel("64 point DFT");
xlabel("f(Hz)");

subplot(5 , 1 , 5);
n1 = 256/8;
f1 = fs*((-n1+1)/2:(n1-1)/2)/n1;
plot(f1 ,fEEG_ds3);
xlim([-70  , 70]);
ylabel("32 point DFT");
xlabel("f(Hz)");


%% Q2 te
% eeg_ds_0  = zeros(1 , 512);
N = 512;
eeg_ds_0= [EEG_ds , zeros(1 , N)];
feeg_ds_0 = abs(fftshift(fft(eeg_ds_0 , N)));

eeg_ds_1= [EEG_ds1 , zeros(1 , 384)];
feeg_ds_1 = abs(fftshift(fft(eeg_ds_1 , N)));

eeg_ds_2= [EEG_ds2 , zeros(1 , 448)];
feeg_ds_2 = abs(fftshift(fft(eeg_ds_2 , N)));

eeg_ds_3= [EEG_ds3 , zeros(1 , 480)];
feeg_ds_3 = abs(fftshift(fft(eeg_ds_3 , N)));


figure();
title("zero padding");
subplot(5 , 1 , 1);
plot(f , abs(Y));
xlim([-70  , 70]);
ylabel("512 point");
xlabel("f(Hz)");

subplot(5 , 1 , 2);
plot(f , feeg_ds_0);
xlim([-70  , 70]);
ylabel("256 point");
xlabel("f(Hz)");

subplot(5 , 1 , 3);

plot(f ,feeg_ds_1);
xlim([-70  , 70]);
ylabel("128 point");
xlabel("f(Hz)");

subplot(5 , 1 , 4);

plot(f ,feeg_ds_2);
xlim([-70  , 70]);
ylabel("64 point");
xlabel("f(Hz)");

subplot(5 , 1 , 5);
plot(f ,feeg_ds_3);
xlim([-70  , 70]);
ylabel("32 point");
xlabel("f(Hz)");


%% Q3 
clc;
clear;
fs  = 250;
load("Data1.mat");
data1 = EEG_Sig;
load("Data2.mat");
data2 = EEG_Sig;
load("Data3.mat");
data3 = EEG_Sig;
load("Electrodes.mat");
labels = Electrodes.labels;


%% Q3 alef
clc;
X = data1;
disp_eeg(X, max(max(abs(X)))  , 250,labels,"Data 1");

X = data2;
disp_eeg(X, max(max(abs(X)))  , 250,labels,"Data 2");

X = data3;
disp_eeg(X, max(max(abs(X)))  , 250,labels,"Data 3");


%% Q3 je(ploting the signal in the time domain)

T = 1/fs;             % Sampling period       
L = 1251;             % Length of signal
t = (0:L-1)*T; 
f = fs*((-L+1)/2:(L-1)/2)/L;

figure();
signal = data1(1 , :);
Y = fftshift(fft(signal));
plot(f,Y)
xlim([-100  , 100]);
xlabel("frequency(Hz)");
title("signal in frequency domain");


%% Q3 je(filtering and ploting after filtering)
wo = 60/(fs/2);  
bw = wo/35;
[b,a] = iirnotch(wo,bw);
filt_signal = filter(b,a,signal);

filt_Y = fftshift(fft(filt_signal));

figure();
subplot(2 , 1 , 1);
plot(f,Y)
xlim([-100  , 100]);
ylabel("freq domain ");

subplot(2 , 1 , 2);
plot(f,filt_Y)
xlim([-100  , 100]);
xlabel("frequency(Hz)");
ylabel("freq domain after notch filter");


%% Q1 function
function [stft, f, t] = outstft(x, window, noverlap, nfft, fs)
x = x(:);
xlen = length(x);
wlen = length(window);
hop = wlen - noverlap; 
k = floor((xlen-noverlap)/hop); 
stft = zeros(nfft,k);
for j=1:k
    x_j = x((j-1)*hop+1:(j-1)*hop+wlen);  
    x_j_hamm = x_j .* window;  
    x_j_fft = fft(x_j_hamm,nfft);  
    stft(:,j) = x_j_fft;  
end
f = [0:nfft-1]*(fs/nfft);
t = [0:hop:(k-1)*hop]*(1/fs);
end
