%% EEG HW5
%% Q1
clc;
clear;
load Ex1.mat;
fs = 250;
t = 0:1/fs:5;

%% Q1 alef
data = zeros(15 , 6 , 5*fs+1);
for i = 1:15
    data(i , : , :) = SSVEP_Signal(: , Event_samples(i):Event_samples(i)+ 5*fs);
end

% plot the data for understanding it better
figure
subplot(2 , 1 , 1)
x(: , :) = data(1 , : , :);
plot(t , x(1 , :));
title("one sample of the signal");
subplot(2 , 1 , 2)
pwelch(x(1 , :))
title("one sample of the signal in frequency domain")
event = [1 1 1 2 2 2 3 3 3 4 4 4 5 5 5];

%% Q1 be
clc;
f1 = 6.5;
f2 = 7.35;
f3 = 8.3;
f4 = 9.6;
f5 = 11.61;
t = 0:1/fs:5;
% t=(1:length(data(1 , 1 , :)))/fs;
yf1 = zeros(6 ,5*fs+1);
yf2 = zeros(6 ,5*fs+1);
yf3 = zeros(6 ,5*fs+1);
yf4 = zeros(6 ,5*fs+1);
yf5 = zeros(6 ,5*fs+1);
for i = 1:3
    yf1(2*i-1 , :) = sin(2*pi*f1*i.*t);
    yf2(2*i-1 , :) = sin(2*pi*f2*i.*t);
    yf3(2*i-1 , :) = sin(2*pi*f3*i.*t);
    yf4(2*i-1 , :) = sin(2*pi*f4*i.*t);
    yf5(2*i-1 , :) = sin(2*pi*f5*i.*t);
end

for i = 1:3
    yf1(2*i , :) = cos(2*pi*f1*i.*t);
    yf2(2*i , :) = cos(2*pi*f2*i.*t);
    yf3(2*i , :) = cos(2*pi*f3*i.*t);
    yf4(2*i , :) = cos(2*pi*f4*i.*t);
    yf5(2*i , :) = cos(2*pi*f5*i.*t);
end


r = zeros(15 , 5 , 6);
maxx = zeros(1 , 5);
result = zeros(1 , 15);
for i = 1:15
    x(: , :) = data(i , : , :);
    [A,B,r(i , 1 , :)] = canoncorr(x' , yf1');
    maxx(1) = max(r(i , 1 , :));
    [A,B,r(i , 2 , :)] = canoncorr(x' , yf2');
    maxx(2) = max(r(i , 2 , :));
    [A,B,r(i , 3 , :)] = canoncorr(x' , yf3');
    maxx(3) = max(r(i , 3 , :));
    [A,B,r(i , 4 , :)] = canoncorr(x' , yf4');
    maxx(4) = max(r(i , 4 , :));
    [A,B,r(i , 5 , :)] = canoncorr(x' , yf5');
    maxx(5) = max(r(i , 5 , :));
    result(i)= find(maxx==max(maxx));
end

acc = sum(event==result)/15;

%% Q1 je

% with one electrode

for j = 1:5 
    f1 = yf1(j , :);
    f2 = yf2(j , :);
    f3 = yf3(j , :);
    f4 = yf4(j , :);
    f5 = yf5(j , :);
    dataa = data(: , j , :);
    for i = 1:15
        k(: , :) = dataa(i , : , :);
        [A,B,r(i , 1 , :)] = canoncorr(k , f1'); 
        maxx(1) = max(r(i , 1 , :));
        [A,B,r(i , 2 , :)] = canoncorr(k , f2');
        maxx(2) = max(r(i , 2 , :));
        [A,B,r(i , 3 , :)] = canoncorr(k , f3');
        maxx(3) = max(r(i , 3 , :));
        [A,B,r(i , 4 , :)] = canoncorr(k , f4');
        maxx(4) = max(r(i , 4 , :));
        [A,B,r(i , 5 , :)] = canoncorr(k , f5');
        maxx(5) = max(r(i , 5 , :));
        result(i)= find(maxx==max(maxx));
    end
    accone(j) = sum(event==result)/15;
end

%with more than one electrode
for j = 2:5
    for t = 1:50
        acc(j , t) = cancorrr(j , data , yf1 , yf2 , yf3 , yf4 , yf5 , event);
    end
end
meanacc = mean(acc , 2);
%% Q1 de
r = zeros(15 , 5 , 6);
maxx = zeros(1 , 5);
result = zeros(1 , 15);
acct = zeros(1 , 12);
for t = 100:100:1200
    yt1 = yf1(: , 1:t);
    yt2 = yf2(: , 1:t);
    yt3 = yf3(: , 1:t);
    yt4 = yf4(: , 1:t);
    yt5 = yf5(: , 1:t);
    for i = 1:15
        l(: , :) = data(i , : , 1:t);
        [A,B,r(i , 1 , :)] = canoncorr(l' , yt1');
        maxx(1) = max(r(i , 1 , :));
        [A,B,r(i , 2 , :)] = canoncorr(l' , yt2');
        maxx(2) = max(r(i , 2 , :));
        [A,B,r(i , 3 , :)] = canoncorr(l' , yt3');
        maxx(3) = max(r(i , 3 , :));
        [A,B,r(i , 4 , :)] = canoncorr(l' , yt4');
        maxx(4) = max(r(i , 4 , :));
        [A,B,r(i , 5 , :)] = canoncorr(l' , yt5');
        maxx(5) = max(r(i , 5 , :));
        result(i)= find(maxx==max(maxx));
    end
    clear yt1 & yt2 & yt3 & yt4 & yt5 & l
    acct(t/100) = sum(event==result)/15;
end

%% Q2
clc;
clear;
load Ex2.mat;
fs = 200;
EC = ec';
EO = eo';
Etest = test_samples';
% ECfft = fft(EC);
% EOfft = fft(EO);

pbandsEC = zeros(4 , 60); %1 - 4  %4 - 8  %8 - 12  %12 - 30
pbandsEO = zeros(4 , 60); %1 - 4  %4 - 8  %8 - 12  %12 - 30
pbandtest = zeros(4 , 60);

for i = 1:60
    %EC
%     a = pwelch(EC( i , :),fs);
    a = abs(fft(ec(:,i))).^2;
    pbandsEC(1 , i) = sum(a(1:40))/40;
    pbandsEC(2 , i) = sum(a(41:80))/40;
    pbandsEC(3 , i) = sum(a(81:130))/50;
    pbandsEC(4 , i) = sum(a(131:300))/170;
    
    %EO
%     a = pwelch(EO( i , :),fs);
    a = abs(fft(eo(:,i))).^2;
    pbandsEO(1 , i) = sum(a(1:40))/40;
    pbandsEO(2 , i) = sum(a(41:80))/40;
    pbandsEO(3 , i) = sum(a(81:130))/50;
    pbandsEO(4 , i) = sum(a(131:300))/170;
    
    %test
%     a = pwelch(Etest( i , :),fs);
    a = abs(fft(test_samples(:,i))).^2;
    pbandtest(1 , i) = sum(a(1:40))/40;
    pbandtest(2 , i) = sum(a(41:80))/40;
    pbandtest(3 , i) = sum(a(81:130))/50;
    pbandtest(4 , i) = sum(a(131:300))/170;
end

X = [pbandsEC pbandsEO]';
Y = [zeros(1 , 60) ones(1 , 60)]'; %EC 0 and EO 1
X_test  = pbandtest';
Y_test = test_labels;

% normalizing

[X,mu,sigma] = zscore(X);
for i = 1:4
    X_test(: ,i) =( X_test(: , i)-mu(i))/sigma(i);
end

mdl = fitlm(X,Y);
Y_pred = predict(mdl,X_test);
mse_mdl = mse(Y_pred , Y_test);

%% Q2 fitlm 10 fold cross validation
indices = crossvalind('Kfold',120 ,10);
ACC = zeros(1 , 10);
SENS = zeros(1 , 10);
SPEC = zeros(1 , 10);

for i = 1:10
    test = (indices == i); 
    train = ~test;
    mdl = fitlm(X(train),Y(train));
    Y_pred = predict(mdl,X(test))>0.5;
    [acc , sens , spec] = confusion(Y(test) , Y_pred);
    ACC(i) = acc;
    SENS(i) = sens;
    SPEC(i) = spec;   
end
macc = mean(ACC);
mspec = mean(SPEC);
msens = mean(SENS);


     
%% Q2 SVM 10 fold cross validation
indices = crossvalind('Kfold',120 ,10);
ACC = zeros(1 , 10);
SENS = zeros(1 , 10);
SPEC = zeros(1 , 10);

for i = 1:10
    test = (indices == i); 
    train = ~test;
    mdl = fitcsvm(X(train),Y(train));
    Y_pred = predict(mdl,X(test));
    Y1_pred = Y_pred>0.5;

    [acc , sens , spec] = confusion(Y(test) , Y1_pred);
    ACC(i) = acc;
    SENS(i) = sens;
    SPEC(i) = spec;   
end
macc = mean(ACC);
mspec = mean(SPEC);
msens = mean(SENS);

%% Q2 test datas (acc and spec and sens  & ROC plot & confusion matrix)
mdl = fitcsvm(X,Y);
Y_pred = predict(mdl,X_test);
mse_mdl = mse(Y_pred , Y_test);

[X1,Y1,T,AUC] = perfcurve(Y_test,Y_pred,1);  %==>  best threshhold is 0.64
figure();
plot(X1 , Y1);
title("ROC data test");

Y1_pred = zeros(60 , 1);
for j = 1:60
    if(Y_pred(j , 1) > 0.64)
        Y1_pred(j , 1) = 1;
    else
        Y1_pred(j , 1) = 0;
    end
end

[acc , sens , spec] = confusion(Y_test , Y1_pred);
figure();
plotconfusion(Y_test',Y1_pred' , "test datas");

%% non linear model
% svmStruct = svmtrain(data(train,:),groups(train),'kernel_function','rbf')
SVMModel = fitcsvm(X,Y,'Standardize',true,'KernelFunction','RBF',...
    'KernelScale','auto');
% CVSVMModel = crossval(SVMModel);
% classLoss = kfoldLoss(CVSVMModel);

indices = crossvalind('Kfold',120 ,10);
ACC = zeros(1 , 10);
SENS = zeros(1 , 10);
SPEC = zeros(1 , 10);

for i = 1:10
    test = (indices == i); 
    train = ~test;
    mdl = fitcsvm(X(train),Y(train),'Standardize',true,'KernelFunction','RBF','KernelScale','auto');
    Y_pred = predict(mdl,X(test));
    Y1_pred = Y_pred>0.5;

    [acc , sens , spec] = confusion(Y(test) , Y1_pred);
    ACC(i) = acc;
    SENS(i) = sens;
    SPEC(i) = spec;   
end
macc = mean(ACC);
mspec = mean(SPEC);
msens = mean(SENS);

%% non linear Q2 test datas (acc and spec and sens  & ROC plot & confusion matrix)
mdl = fitcsvm(X,Y,'KernelFunction','RBF');
Y_pred = predict(mdl,X_test);
mse_mdl = mse(Y_pred , Y_test);

[X1,Y1,T,AUC] = perfcurve(Y_test,Y_pred,1);  %==>  best threshhold is 0.64
figure();
plot(X1 , Y1);
title("ROC data test");

Y1_pred = zeros(60 , 1);
for j = 1:60
    if(Y_pred(j , 1) > 0.5)
        Y1_pred(j , 1) = 1;
    else
        Y1_pred(j , 1) = 0;
    end
end

[acc , sens , spec] = confusion(Y_test , Y1_pred);
figure();
plotconfusion(Y_test',Y1_pred' , "test datas");

%% Q3
clc;
clear;
load Ex3.mat;
fs = 256;
t = 1/fs:1/fs:1;
%% Q3 alef
clc;
x_train = TrainData - mean(TrainData , 2);
x_test = TestData - mean(TestData , 2);
y_train = TrainLabel;
y0 = find(y_train==0);
y1 = find(y_train==1);
x0 = x_train(: , : , y0);
x1 = x_train(: , : , y1);


C0 = cov(mean(x0 , 3)');
C1 = cov(mean(x1 , 3)');

[W,D] = eig( C0 , C1 );

Wcsp =[ W(: , 1) , W(: , end)];
mx_train(: , :) = mean(x_train , 2);
for i = 1:165
    Y(: , : , i) = Wcsp'*x_train(: , : , i);
end
Y0 = Y(: , : , y0);
Y1 = Y(: , : , y1);

subplot(2 , 1 , 1)
plot(t , mean(Y0 , 3));
legend("bottom CSP filter" , "Top SCP filter");
ylabel("for label 0");
xlabel("t");
title("mean of the experiments")
subplot(2 , 1 , 2)
plot(t , mean(Y1 , 3));
legend("bottom CSP filter" , "Top SCP filter");
ylabel("for label 1");
xlabel("t");


%% Q3 be
load('AllElectrodes.mat');
elocsX = zeros(1 , 30);
elocsY = zeros(1 , 30);
Electrodes = AllElectrodes([37,47,45,15,13,48,50,52,18,32,7,5,38,40,42,10,55,23,22,21,20,31,57,58,59,60,26,63,27,64]);

for i = 1:30
    elocsX(1,i) = Electrodes(1,i).X;
    elocsY(1,i) = Electrodes(1,i).Y;
    labels{i} = Electrodes(1,i).labels;
end
figure
plottopomap(elocsX',elocsY',labels,W(: , 1));
title('first filter');
figure
plottopomap(elocsX',elocsY',labels,W(: , end));
title('last filter');
%% Q3 je 1
indices = crossvalind('Kfold',165 ,3);
clear Y & Wcsp & M1 & M;
for i = 1:3
    test1 = (indices == i); 
    train1 = ~test1;
    TTrainData = TrainData(: , : , train1);
    TTestData = TrainData(: , : , test1);
    y_train = TrainLabel(train1);
    y_test = TrainLabel(test1);
    %%%%%%%%%%%%%%%%%%%%%%%%%
    x_train = TTrainData - mean(TTrainData , 1);
    x_test = TTestData - mean(TTestData , 1);
    y0 = find(y_train==0);
    y1 = find(y_train==1);
    x0 = x_train(: , : , y0);
    x1 = x_train(: , : , y1);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    C0 = cov(mean(x0 , 3)');
    C1 = cov(mean(x1 , 3)');

    [W,D] = eig( C0 , C1 );

    Wcsp =[ W(: , 1) , W(: , end)];

    for j = 1:length(x_train(1 , 1 , :))
        Y(: , : , j) = Wcsp'*x_train(: , : , j);
    end
    M1 = var(Y ,0 ,  2);
    M(: , :) = M1;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
    mdl = fitlm(M(train1),y_train);
    Y_pred = predict(mdl,M(test1));
%     performance(i) = 1 - (sum((Y_pred' - y_test).^2))/length(y_test(1 , :));
    performance(i) = mse(Y_pred , y_test');
end

%% Q3 je 2

for j = 1:7
    for i = 1:3
        test1 = (indices == i); 
        train1 = ~test1;
        TTrainData = TrainData(: , : , train1);
        TTestData = TrainData(: , : , test1);
        y_train = TrainLabel(train1);
        y_test = TrainLabel(test1);
        %%%%%%%%%%%%%%%%%%%%%%%%%
        x_train = TTrainData - mean(TTrainData , 1);
        x_test = TTestData - mean(TTestData , 1);
        y0 = find(y_train==0);
        y1 = find(y_train==1);
        x0 = x_train(: , : , y0);
        x1 = x_train(: , : , y1);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        C0 = cov(mean(x0 , 3)');
        C1 = cov(mean(x1 , 3)');

        [W,D] = eig( C0 , C1 );
        clear Wcsp & Y & M1 & M;
        Wcsp =cat(2 ,  W(: , 1:j) , W(: , end-j+1:end));

        for t = 1:length(x_train(1 , 1 , :))
            Y(: , : , t) = Wcsp'*x_train(: , : , t);
        end
        M1 = var(Y ,0 ,  2);
        M(: , :) = M1;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
        mdl = fitlm(M(train1),y_train);
        Y_pred = predict(mdl,M(test1));
    %     performance(i) = 1 - (sum((Y_pred' - y_test).^2))/length(y_test(1 , :));
        mmmse(i) = mse(Y_pred , y_test');
    end
    MSE(j) = mean(mmmse);

end
%% Q3 part de
clc;
clc
load Ex3.mat;
fs = 256;
t = 1/fs:1/fs:1;
clear Wcsp & Y;
x_train = TrainData - mean(TrainData , 2);
x_test = TestData - mean(TestData , 2);
y_train = TrainLabel;

y0 = find(y_train==0);
y1 = find(y_train==1);
x0 = x_train(: , : , y0);
x1 = x_train(: , : , y1);


C0 = cov(mean(x0 , 3)');
C1 = cov(mean(x1 , 3)');

[W,D] = eig( C0 , C1 );

Wcsp =[ W(: , 1) , W(: , end)];
clear M1 & M & Y;
for i = 1:165
    Y(: , : , i) = Wcsp'*x_train(: , : , i);
end

M1 = var(Y ,0 ,  2);
M(: , :) = M1;
mdl = fitcsvm(M',TrainLabel);
clear M1 & M & Y;
for i = 1:45
    Y(: , : , i) = Wcsp'*x_test(: , : , i);
end
M1 = var(Y ,0 ,  2);
M(: , :) = M1;
Y_pred = predict(mdl,M');
TestLabel = Y_pred;
save TestLabel.mat
%% function

function [acc , sens , spec] = confusion(y_test , y_pred)
    lenn = length(y_test(: , 1));
    tp = 0;
    tn = 0;
    fp = 0;
    fn = 0;
    for i = 1:lenn
        if(y_test(i)==1 && y_pred(i)==1)
            tp = tp+1;
        elseif(y_test(i)==0 && y_pred(i)==0)
            tn = tn+1;
        elseif(y_test(i)==0 && y_pred(i)==1)
            fp = fp+1;
        elseif(y_test(i)==1 && y_pred(i)==0)
            fn = fn+1;
        end 
    end
    acc = (tp + tn)/lenn;
    sens = tp/(tp + fn);
    spec = tn/(tn + fp); 
end

function acc = cancorrr(j , data , yf1 , yf2 , yf3 , yf4 , yf5 , event)
%     r = zeros(15 , 5 , 6);
    maxx = zeros(1 , 5);
    result = zeros(1 , 15);
    
    p = randperm(6,j);

    dataa = data(: , p , :);
    f1 = yf1(1:j , :);
    f2 = yf2(1:j , :);
    f3 = yf3(1:j , :);
    f4 = yf4(1:j , :);
    f5 = yf5(1:j , :);

    for i = 1:15
        x(: , :) = dataa(i , : , :);
        [A,B,r(i , 1 , :)] = canoncorr(x' , f1');        
        maxx(1) = max(r(i , 1 , :));
        [A,B,r(i , 2 , :)] = canoncorr(x' , f2');
        maxx(2) = max(r(i , 2 , :));
        [A,B,r(i , 3 , :)] = canoncorr(x' , f3');
        maxx(3) = max(r(i , 3 , :));
        [A,B,r(i , 4 , :)] = canoncorr(x' , f4');
        maxx(4) = max(r(i , 4 , :));
        [A,B,r(i , 5 , :)] = canoncorr(x' , f5');
        maxx(5) = max(r(i , 5 , :));
        result(i)= find(maxx==max(maxx));
    end
    acc = sum(event==result)/15;
end