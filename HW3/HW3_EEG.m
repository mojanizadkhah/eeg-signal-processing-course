%% EEG HW3
%% Q1 alef
clc;
clear;
load("Ex1.mat");
x = X;
scatter3(x(1 , :) , x(2 , :) , x(3 , :), 'filled' , 'MarkerEdgeColor','g' );
xlabel("1");
ylabel("2");
zlabel("3");
title("experiment 1 ");

%% Q1 be
x = x - mean(x , 2);
covx = cov(x');
[u , landa] = eig(covx);
temp = landa(3 , 3)
landa(3 , 3) = landa(1 , 1);
landa(1 , 1) = temp;

for i = 1:3
    B(i , i) = 1./sqrt(landa(i , i));
end

temp = u(: , 3);
u(: , 3) = u(: , 1);
u(: , 1) = temp;

A = u';
D = B*A;
Cy = D*covx*D'; % it is almost eqal to I .just for checking.
y = D*x;
scatter3(y(1 , :) , y(2 , :) , y(3 , :), 'filled' , 'MarkerEdgeColor','b' );
title("experiment 1 after applying PCA");

%% Q1 je
[um , ym1] =  pca(x');
ym = ym1';
scatter3(ym(1 , :) , ym(2 , :) , ym(3 , :), 'filled' , 'MarkerEdgeColor','y' );
title("experiment 1 after applying PCA of Matlab");


%% just for checking Q1
scatter3(ym(1 , :) , ym(2 , :) , ym(3 , :), 'filled' , 'MarkerEdgeColor','b' );
hold on
scatter3(x(1 , :) , x(2 , :) , x(3 , :), 'filled' , 'MarkerEdgeColor','g' );
legend("after PCA" , "before PCA");
% mse = sum(sum( (y - ym).^2))/3000;

%% Q1 de
[us,ss,vs] = svd(x);
ys = ss*vs;

scatter3(ys(1 , :) , ys(2 , :) , ys(3 , :), 'filled' , 'MarkerEdgeColor','y' );
title("experiment 1 after applying SVD of Matlab");

mse = sum(sum( (y - ys).^2))/3000;

%% Q2 alef
clc;
clear;
load("Ex2");
x = X_org;
poworg = sqrt(sum(sum((X_org).^2)));
n1 = X_noise_1;
n2 = X_noise_2;
n3 = X_noise_3;
n4 = X_noise_4;
n5 = X_noise_5;

signal = zeros(4 , 32 , 10240);
n = n2;
ps = var(x);
pn = var(n);
for i = [-5 , -10 , -15 , -20 ]
    signal(-i/5 , : , :) = x + n*(ps/(pn*(10^(i/10))));
end
for i = 1:32
    ElecNameS(i) = i;
end


X = x;
offset = max(abs(X(:))) ;
feq = 250 ;
load("Electrodes_new");
ElecName  = Electrodes_new.labels;
disp_eeg(X,offset,feq,ElecName , "without noise");


X(: , :) = signal(3 , : , :);
offset = max(abs(X(:))) ;
feq = 250 ;
load("Electrodes_new");
ElecName  = Electrodes_new.labels;
disp_eeg(X,offset,feq,ElecName , "noisy 15Db");

%% Q2 be and je and he

%PCA
sig = X;
[Upca,y_pca] = pca(sig');
ypca = y_pca';


offset = max(abs(ypca(:))) ;
feq = 250 ;
load("Electrodes_new");
ElecName  = Electrodes_new.labels;
disp_eeg(ypca,offset,feq,ElecName , "noisy 15Db sources after PCA");

% Q2 je PCA
ypca(1:2 , :) = 0;
ypca(7:32 , :) = 0;
xden = pinv(Upca)*y_pca';


offset = max(abs(xden(:))) ;
feq = 250 ;
load("Electrodes_new");
ElecName  = Electrodes_new.labels;
disp_eeg(xden,offset,feq,ElecName , "signal after denoising with PCA");

% Q2 he PCA
figure(1)
subplot(6 , 1 , 1)
plot(X_org(13 , :))
ylabel("13 orginal")
title("PCA");
subplot(6 , 1 , 2)
plot(sig(13 , :))
ylabel("13 noisy")

subplot(6 , 1 , 3)
plot(xden(13 , :))
ylabel("13 denoised")

subplot(6 , 1 , 4)
plot(X_org(24 , :))
ylabel("24 orginal")

subplot(6 , 1 , 5)
plot(sig(24 , :))
ylabel("24 noisy")

subplot(6 , 1 , 6)
plot(xden(24 , :))
ylabel("24 denoised")


%%
%EMGsorsep CCA
sig = X;
[sourcescca,A_xcca,autocorcca]= EMGsorsep(sig);
offset = max(abs(sourcescca(:))) ;
feq = 250 ;
disp_eeg(sourcescca,offset,feq,ElecNameS , "noisy 15Db sources after CCA");

%  Q2 je cca
sourcescca(8:32 , :) = 0;
xden = A_xcca*sourcescca;
offset = max(abs(xden(:))) ;
feq = 250 ;
load("Electrodes_new");
ElecName  = Electrodes_new.labels;
disp_eeg(xden,offset,feq,ElecName , "signal after denoising with CCA");

% Q2 he CCA
figure(1)
subplot(6 , 1 , 1)
plot(X_org(13 , :))
ylabel("13 orginal")
title("CCA");
subplot(6 , 1 , 2)
plot(sig(13 , :))
ylabel("13 noisy")

subplot(6 , 1 , 3)
plot(xden(13 , :))
ylabel("13 denoised")

subplot(6 , 1 , 4)
plot(X_org(24 , :))
ylabel("24 orginal")

subplot(6 , 1 , 5)
plot(sig(24 , :))
ylabel("24 noisy")

subplot(6 , 1 , 6)
plot(xden(24 , :))
ylabel("24 denoised")


%%
% ICA
[F,W,K]=COM2R(sig,32);
yica = W*X;
offset = max(abs(yica(:))) ;
feq = 250 ;
load("Electrodes_new");
disp_eeg(yica,offset,feq,ElecNameS , "noisy 15Db sources after ICA");

% Q2 je ICA
yica(2:9 , :) = 0;
yica(11:12 , :) = 0;
yica(14 , :) = 0;
yica(16:32 , :) = 0;

xden = F*yica;
offset = max(abs(xden(:))) ;
feq = 250 ;
load("Electrodes_new");
ElecName  = Electrodes_new.labels;
disp_eeg(xden,offset,feq,ElecName , "signal after denoising with ICA");

% Q2 he CCA
figure(1)
subplot(6 , 1 , 1)
plot(X_org(13 , :))
ylabel("13 orginal")
title("CCA");
subplot(6 , 1 , 2)
plot(sig(13 , :))
ylabel("13 noisy")

subplot(6 , 1 , 3)
plot(xden(13 , :))
ylabel("13 denoised")

subplot(6 , 1 , 4)
plot(X_org(24 , :))
ylabel("24 orginal")

subplot(6 , 1 , 5)
plot(sig(24 , :))
ylabel("24 noisy")

subplot(6 , 1 , 6)
plot(xden(24 , :))
ylabel("24 denoised")
%% Q2 he
for i = [-5 , -10 , -15 , -20 ]
    signal(-i/5 , : , :) = x + n3*(ps/(pn*(10^(i/10))));
end

error = zeros(3 , 4);
for i = 1:4
    X(: , :) = signal(i , : , :);
    
    % pca
    sig = X;
    [Upca,y_pca] = pca(sig');
    ypca = y_pca';
    ypca(1:2 , :) = 0;
    ypca(7:32 , :) = 0;
    xdenpca = pinv(Upca)*y_pca';
    error(1 , i) = mrmse(X_org,xdenpca);
    

    % cca
    [sourcescca,A_xcca,autocorcca]= EMGsorsep(sig);
    sourcescca(8:32 , :) = 0;
    xdencca = A_xcca*sourcescca;
    error(2 , i) = mrmse(X_org,xdencca);

    % ica
    [F,W,K]=COM2R(sig,32);
    yica = W*X;
    yica(2:9 , :) = 0;
    yica(11:12 , :) = 0;
    yica(14 , :) = 0;
    yica(16:32 , :) = 0;
    xdenica = F*yica;
    error(3 , i) = mrmse(X_org,xdenica);
end

%% Q3 
clc;
clear;
feq = 250;
load("NewData1");
data1 = EEG_Sig;
load("NewData2");
data2 = EEG_Sig;
load("NewData3");
data3 = EEG_Sig;
load("NewData4");
data4  = EEG_Sig;
%% Q3 alef
X = data1;
offset = max(abs(X(:))) ;
feq = 250 ;
load("Electrodes_new");
ElecName  = Electrodes_new.labels;
disp_eeg(X,offset,feq,ElecName , "Data 1");

X = data2;
offset = max(abs(X(:))) ;
feq = 250 ;
load("Electrodes_new");
ElecName  = Electrodes_new.labels;
disp_eeg(X,offset,feq,ElecName , "Data 2");

X = data3;
offset = max(abs(X(:))) ;
feq = 250 ;
load("Electrodes_new");
ElecName  = Electrodes_new.labels;
disp_eeg(X,offset,feq,ElecName , "Data 3");

X = data4;
offset = max(abs(X(:))) ;
feq = 250 ;
load("Electrodes_new");
ElecName  = Electrodes_new.labels;
disp_eeg(X,offset,feq,ElecName , "Data 4");


%% Q3 je

[F1,W1,K1]=COM2R(data1,21);
[F2,W2,K2]=COM2R(data2,21);
[F3,W3,K3]=COM2R(data3,21);
[F4,W4,K4]=COM2R(data4,21);

%% Q3 de and he data1

% data in time 
S = W1*data1;
offset = max(abs(S(:))) ;
feq = 250 ;
for i = 1:32
    ElecNameS(i) = i;
end
disp_eeg(S,offset,feq,ElecNameS , "Data 1 time");

%% pwelch data in frequency
figure(1)
for i = 1:21    
    pxx = pwelch(S(i , :) , 250 );
    hold on;

    plot(pxx);
    
end
title("Data 1 frequency");

%% top map
load("Electrodes");
elocsX = Electrodes.X;
elocsY = Electrodes.Y;
elabels  = Electrodes.labels;
figure(2)
for i = 1:21
    subplot(3 , 7 , i)
    plottopomap(elocsX,elocsY,elabels,W1(: , i))
end
title("Data 1 topmap");
%
SelSources1 = [1 2 3 5 6 7 8 9 11 12 13 14 15 16 17 18];
xden1 = F1(: , SelSources1)*S(SelSources1 , :);

signal = xden1
offset = max(abs(signal(:))) ;
feq = 250 ;
load("Electrodes_new");
ElecName  = Electrodes_new.labels;
disp_eeg(signal,offset,feq,ElecNameS , "denoised data 1");

%% Q3 de and he data2

% data in time 
S = W2*data2;
offset = max(abs(S(:))) ;
feq = 250 ;
for i = 1:32
    ElecNameS(i) = i;
end

disp_eeg(S,offset,feq,ElecNameS , "Data 2 time");

%% pwelch data in frequency
figure(2)
for i = 1:21    
    pxx = pwelch(S(i , :) , 250 );
    hold on;

    plot(pxx);
end
title("Data 2 frequency");

%% top map
load("Electrodes");
elocsX = Electrodes.X;
elocsY = Electrodes.Y;
elabels  = Electrodes.labels;
figure(3)
for i = 1:21
    subplot(3 , 7 , i)
    plottopomap(elocsX,elocsY,elabels,W2(: , i))
end
title("Data 2 topmap");
%%
S = W2*data2;
SelSources2 = [  3 4 5  8 9 10 11 12 13 14 15 16 17 18 20 21];
xden2 = F2(: , SelSources2)*S(SelSources2 , :);

signal = xden2;
offset = max(abs(signal(:))) ;
feq = 250 ;
load("Electrodes_new");
ElecName  = Electrodes_new.labels;
disp_eeg(signal,offset,feq,ElecName , "denoised data 2");

%% Q3 de and he data3

% data in time 
S = W3*data3;
offset = max(abs(S(:))) ;
feq = 250 ;
for i = 1:32
    ElecNameS(i) = i;
end

disp_eeg(S,offset,feq,ElecNameS , "Data 3 time");

%% pwelch data in frequency
figure(2)
% pxx = zeros(1 , 129);
for i = 1:21    
    pxx = pwelch(S(i , :) , 250 );
    hold on;

    plot(pxx);
end
title("Data 3 frequency");

%% top map
load("Electrodes");
elocsX = Electrodes.X;
elocsY = Electrodes.Y;
elabels  = Electrodes.labels;
figure(3)
for i = 1:21
    subplot(3 , 7 , i)
    plottopomap(elocsX,elocsY,elabels,W3(: , i))
end
title("Data 3 topmap");
%%
S = W3*data3;
SelSources3 = [ 2 3 4 5 6 8 9 10 11 12 13 14 15 16 17 18 20 21];
xden3 = F3(: , SelSources3)*S(SelSources3 , :);

signal = xden3;
offset = max(abs(signal(:))) ;
feq = 250 ;
load("Electrodes_new");
ElecName  = Electrodes_new.labels;
disp_eeg(signal,offset,feq,ElecName , "denoised data 3");

%%
function rrmse = mrmse(xorg,xden)
    [a , b] = size(xden);
    if(b==10239)
        xdenf = zeros(32 , 10240);
        xdef(: , 1:10239) = xden;
    elseif(b==10240)
        xdenf = xden;
    end
    porg = sqrt(sum(sum(xorg.^2)));
    pmin = sqrt(sum(sum((xdenf - xorg).^2)))
    rrmse = pmin/porg;
end
