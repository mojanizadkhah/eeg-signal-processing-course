%%HW4_EEG_Matlab
%% Q1
clc;
clear;
load("Ex1_data.mat");
fs = 100;
t = 1/fs:1/fs:100;
len = length(X_org(1 , :));

for i = 1:8
    sceeg(i) =  ["X_org  " + num2str(i)];
end
disp_eeg(X_org,max(max(X_org)),200,sceeg,"X orginal");

%% Q1 alef GEVD
x = X_org;
tau = 400;
Cx = EX(x , x);

a1 = x(: , 1:end-tau);
a2 = x(: , tau+1:end);
Pax = EX(a1 , a2);
Px = 0.5*(Pax + Pax');
[V,D,W] = eig(Px,Cx);
w1 = V(: , 1:8);
S1 = w1'*x;



% w2 = V(: , 1);
% w3 = [w2 , w2 , w2 , w2 , w2 , w2 , w2 , w2];
% S1 = w3'*x;
mse_alef = mse(S1 , X1);
rrmse_alef = mrmse(X1,S1);
for i = 1:8
    sc(i) =  ["source" + num2str(i)];
end
disp_eeg(S1,max(max(S1)),200,sc,"source S1 GEVD");


for i = 1:8
    sceeg(i) =  ["X1  " + num2str(i)];
end
disp_eeg(X1,max(max(X1)),200,sceeg,"X1");
%% Q1 be
x = X_org;
Cx = EX(x , x);
mse_b = zeros(1 , 401);

for tau = 300:1:700
    a1 = x(: , 1:end-tau);
    a2 = x(: , tau+1:end);
    Pax = EX(a1 , a2);
    Px = 0.5*(Pax + Pax');
    [V,D,W] = eig(Px,Cx);
    w1 = V(: , 1:8);
    S1 = w1'*x;

    mse1(1 , tau-300+1) = mrmse(X1,S1);
    
end

tauu = find(mse1==min(mse1));
rrmse_b = min(mse1);
% 399 , 698 are the answers
%% Q1 je

Px = EXburst(x , x , T1 , 0);
[V,D,W] = eig(Px,Cx);
w1 = V(: , 1:8);
S2 = w1'*x;



for i = 1:8
    sc(i) =  ["source" + num2str(i)];
end
disp_eeg(S2,max(max(S2)),200,sc,"source S2 GEVD");


for i = 1:8
    sceeg(i) =  ["X2  " + num2str(i)];
end
disp_eeg(X2,max(max(X2)),200,sceeg,"X2");

% S22 = repmat(S2 , 8);
mse_je = mse(S2, X2);
rrmse_je = mrmse(X2,S2);

%% Q1 de
Px = EXburst(x , x , T2 , 3);
[V,D,W] = eig(Px,Cx);
w1 = V(: , 1:8);
S2 = w1'*x;
mse_de = mse(S2 , X2);
rrmse_de = mrmse(X2,S2);
% for i = 1:8
%     sc(i) =  ["source" + num2str(i)];
% end
% disp_eeg(S2,max(max(S2)),200,sc,"source S2 GEVD(part de)");
% 
% 
% for i = 1:8
%     sceeg(i) =  ["X2  " + num2str(i)];
% end
% disp_eeg(X2,max(max(X2)),200,sceeg,"X2");
%% Q1 he
xf = fft(x);
Cx = EX(xf , xf);

[b,a] = cheby1(10,5,[0.1 0.15],'bandpass');
xfilltered = filter(b,a,xf);
Px =  EX(xfilltered , xfilltered);

[V,D,W] = eig(Px,Cx);
w1f = V(: , 1:8);
% w1 = ifft(w1f);
S3 = w1f'*x;
mse_he = abs(mse(S3 , X3));
rrmse_he = abs(mrmse(X3,S3));
% for i = 1:8
%     sc(i) =  ["source" + num2str(i)];
% end
% disp_eeg(abs(fft(S3)),max(max(abs(fft(S3)))),200,sc,"source S3 GEVD");
% 
% for i = 1:8
%     sceeg(i) =  ["X3  " + num2str(i)];
% end
% disp_eeg(X3,max(max(X3)),200,sceeg,"X3");

%% Q1 ve

xf = fft(x);
Cx = EX(xf , xf);

mse1 = zeros(1 , 4);
for i = 1:1:5
    [b,a] = cheby1(10,5,[i/20 (i+1)/20],'bandpass');
    xf = fft(x);
    xfilltered = filter(b,a,xf);
    Px =  EX(xfilltered , xfilltered);

    [V,D,W] = eig(Px,Cx);
    w1f = V(: , 1:8);
    w1 = ifft(w1f);
    S3 = w1'*x;
    mse1(1 , i) = abs(mrmse(X3 , S3));
end

bpf = find(mse1==min(mse1)); % 0.15 to 0.2
rrmse_ve = min(mse1); 

%% Q1 DSS
clc;
clear all;
load("Ex1_data.mat");
fs = 100;
t = 1/fs:1/fs:100;
len = length(X_org(1 , :));
x = X_org;

%% Q1 alef DSS

[x , D] = white(x);
p = 8;
len = length(x(1 , :));
w = rand(p, 1);
rp = zeros(1 , len);
tau = 400;
L = floor(len/tau);

for i = 1:1000
    
    % E new
    for j = 1:len
        rp(1 , j) = w'*x(: , j);
    end
    
    sum = 0;
    for i = 1:L
        sum = sum +rp(1 ,  1+(i-1)*tau : tau*i );    
    end

    rp = repmat((sum/L),1 , L);

    % M
        w = (x*rp');
        
        A = w(: , 1:end-1);
        worth = w - A*A'*w;
        w = worth;

        w = w/norm(w);
end

X1den = inv(D)*w*rp;
mse_alef = mse(X1den , X1);
rrmse_alef = mrmse(X1,X1den);
%% Q1 be DSS

x = X_org;
[x , D] = white(x);
p = 8;

tauu = [300:20:700];
lenn = length(tauu);
mse1 = zeros(1 , lenn);
len = length(x(1 , :));

for tau = tauu
    w = rand(p, 1);
    L = floor(len/tau);
    rp = zeros(1 , len);

    for i = 1:500
    
    % E new
        for j = 1:len
            rp(1 , j) = w'*x(: , j);
        end

        sum = 0;
        for j = 1:L
            sum = sum +rp(1 ,  1+(j-1)*tau : tau*j );    
        end
        rpp = repmat((sum/L),1 , L);
        
        a = length(rpp(1 , :));
        if(a ~= 10000)
            rp = [rpp , rp(: , a+1:end)];
        else
            rp = rpp;
        end
    % M
            w = (x*rp');

            A = w(: , 1:end-1);
            worth = w - A*A'*w;
            w = worth;

            w = w/norm(w);
    end

    X1den = inv(D)*w*rp;
    mse1(1 , (tau-300)/20 +1) = mrmse(X1,X1den);   
    clear rp;
    clear w;
end

tauu = find(mse1==min(mse1));
rrmse_b = min(mse1);

%% Q1 je DSS
clc;
clear;
load("Ex1_data.mat");
fs = 100;
t = 1/fs:1/fs:100;
x = X_org;
[x , D ]= white(x);
len = length(x(1 , :));
p = 8;
w = rand(p, 1);
rp = zeros(1 , len);
for i = 1:1000    
    % E
    for j = 1:len
        rp(1 , j) = w'*x(: , j);
    end
    rp(1 , :) = rp(1 , :).*T1(1 , :);

    % M
        w = (x*rp');
        A = w(: , 1:end-1);
        worth = w - A*A'*w;
        w = worth;
        w = w/norm(w);
end

X2den = inv(D)*w*rp;
mse_je = mse(X2den , X2);
rrmse_je = mrmse(X2,X2den);
%% Q1 de DSS

x = X_org;
[x , D ]= white(x);
len = length(x(1 , :));
p = 8;
w = rand(p, 1);
rp = zeros(1 , len);
T3 = T2/2 + 0.5;

for i = 1:1000    
    % E
    for j = 1:len
        rp(1 , j) = w'*x(: , j);
    end
    rp(1 , :) = rp(1 , :).*T3(1 , :);

    % M
        w = (x*rp');
        A = w(: , 1:end-1);
        worth = w - A*A'*w;
        w = worth;
        w = w/norm(w);
end

X2den = inv(D)*w*rp;
mse_de = mse(X2den , X2);
rrmse_de = mrmse(X2,X2den);
%% Q1 he DSS

x = X_org;
[x , D ]= white(x);
xf = fft(x);
len = length(xf(1 , :));
p = 8;
w = rand(p, 1);
rp = ones(1 , len);

for i = 1:500   
    % E
    for j = 1:len
        rp(1 , j) = w'*xf(: , j);
    end

    [b,a] = cheby1(10,5,[0.1 0.15],'bandpass');
%     rp = filter(b,a,rp);

    % M
        w = (xf*rp');
        A = w(: , 1:end-1);
        worth = w - A*A'*w;
        w = worth;
        w = w/norm(w);
end
rp = ifft(rp);
X3den = inv(D)*w*rp;
mse_he = norm(mse(X3den , X3));
rrmse_he = norm(mrmse(X3,X3den));
%% Q1 ve DSS

x = X_org;
[x , D ]= white(x);
xf = fft(x);
len = length(xf(1 , :));
p = 8;
w = rand(p, 1);
rp = zeros(1 , len);

mse1= zeros(1, 5);
for t = 1:1:5
    for i = 1:500   
        % E
        for j = 1:len
            rp(1 , j) = w'*xf(: , j);
        end

        [b,a] = cheby1(10,5,[t/20 (t+1)/20],'bandpass');
%         rpf = filter(b,a,rpf);

        % M
            w = (xf*rp');
            A = w(: , 1:end-1);
            worth = w - A*A'*w;
            w = worth;
            w = w/norm(w);
    end
    rp = ifft(rp);
    X3den = inv(D)*w*rp;
    mse1(1 , t) = abs(mrse(X3den , X3));
end

bpf = find(mse1==min(mse1)); % 0.15 to 0.2
rrmse_ve = min(mse1); 

%% Q2
clc;
clear;
fs = 200;
load("pure.mat");
load("contaminated");
load("Electrodes.mat");
electrodes = Electrodes.labels(1 , 1:19);

%% Q2 alef
t = 0:1/fs:28;

disp_eeg(pure,max(max(pure)),200,electrodes,"pure signal");
disp_eeg(contaminated,max(max(pure)),200,electrodes,"contaminated signal");

% disp_eeg(contaminated-pure,max(max(contaminated-pure)),200,electrodes,"contaminated signal");
rrmse_tot = mrmse(pure,contaminated);

T1 = zeros(19 , 5601);
T1(:  , 1.36*200:3.6*200) = 1;
% T1(:  , 10.66*200:11.89*200) = 1;
T1(:  , 16*200:16.56*200) = 1;
T1(:  , 22.05*200:22.76*200) = 1;

%% Q2 GEVD

x = contaminated;
Ctxx = EX(x.*T1 , x.*T1);
Cxx = EX(x , x);


[V,D,W] = eig(Ctxx,Cxx);
w1 = V;
S1 = w1'*x;

for i = 1:19
    sc(i) =  ["source" + num2str(i)];
end
disp_eeg(S1,max(max(S1)),200,sc,"sources");
S1(19 , :) = 0;
xden = w1'*S1;
disp_eeg(xden,max(max(xden)),200,electrodes,"signal after cleaning with GEVD");

rrmse_gevd = mrmse(pure,xden);
%% Q2 DSS
clc;
x = contaminated;
[x , D ]= white(x);
len = length(x(1 , :));
p = 19;
w = rand(19, 1);
rp = zeros(1 , len);
T2 = ~T1;

for i = 1:1000    
    % E
    for j = 1:len
        rp(1 , j) = w'*x(: , j);
    end
    rp(1 , :) = rp(1 , :).*T1(1 , :);
    % M
        w = (x*rp');
        A = w(: , 1:end-1);
        worth = w - A*A'*w;
        w = worth;
        w = w/norm(w);
end

noise = inv(D)*w*rp;
disp_eeg(noise,max(max(noise)),200,sc,"source noise");

xden = contaminated - noise;
disp_eeg(xden,max(max(xden)),200,electrodes,"signal after cleaning with DSS");

rrmse_dss = mrmse(pure,xden);



%% functions
function Cxy = EX(x , y)
    len = length(x(1 , :));
    C = 0;
    for i = 1:len
        C = C+ x(: , i)*y(: , i)';
    end
    Cxy = C./len;
end

function Cxy = EXburst(x , y , t , ro)
    len = length(x(1 , :));
    lent = length(t(1 , :));
    C = 0;
    % I made ro for T2 part
    t = t + ro;
    t = t/max(t);
    for i = 1:len
        C = C + t(: , i)* x(: , i)*y(: , i)';
    end
    Cxy = C./lent;
end

function rrmse = mrmse(xorg,xden)
    porg = sqrt(sum(sum(xorg.^2)));
    pmin = sqrt(sum(sum((xden - xorg).^2)));
    rrmse = pmin/porg;
end

function [y , D] = white(x)
    x = x - mean(x , 2);
    covx = cov(x');

    [u , landa] = eig(covx);
    len = length(x(: , 1));

    Landa = zeros(len);
    for i = 1:len
        Landa(i , i) = landa(len-i+1 , len - i+1);
        B(i , i) = 1./sqrt(Landa(i , i));
        temp(: , i) = u(: , len-i+1);
    end
    A = u';
    D = B*A;
    y = D*x;
end
