%% Q1
clc;
clear;
load("ERP_EEG.mat");
fs = 240;
t = 1/fs:1/fs:1;


%% Q1 alef 1
erp = ERP_EEG;
N = 100:100:2500;
meansig = zeros(240 , 1);
meansig(: , 1) = mean(erp(: , 1:N) , 2);

for N = 100:100:2500
    meansig(: , 1) = mean(erp(: , 1:N) , 2);
    plot(t , meansig);
    hold on;   
end
legend("100" , "200" , "300" , "400" , "500");
xlabel("t(ms)");
ylabel("ERP response");


%% Q1 alef 2
maxx = zeros(1 , 2550);

for i = 1:2550
    meansig(: , 1) = mean(erp(: , 1:i) , 2);
    maxx(1 , i) = max(abs(meansig));
end

plot(1:1:2550 , maxx);
xlabel("number of trials(that we calculated the mean from)");
ylabel("maximum absolute of the amplitude");


%% Q1 alef 3
meansig = zeros(240 , 2550);
rms = zeros(1 , 2550);
meansig(: , 1) = mean(erp(: , 1) , 2);

for i = 2:2550
    meansig(: , i) = mean(erp(: , 1:i) , 2);
    rms(1 , i) = sqrt(sum(   (meansig(: , i) - meansig(: , i-1)).^2 ));
end

plot(1:1:2550 , rms);
xlabel("number of trials(that we calculated the mean from)");
ylabel("RMS Error");
%% Q1 be
load("ERP_EEG.mat");
erp = ERP_EEG;
signal = erp(1:200 , :);
meansig = mean(erp(: , 1:200) , 2);
shifts = zeros(1 , 200);
maxshift = 0.05*fs; 
y = zeros(240 , 1);
plot(t , meansig , 'c*');
hold on;

for round = 1:100
    meansig = mean(erp(: , 1:200) , 2);
    for i = 1:200
       [y1,lags] = xcorr(erp(:,i),meansig);
       max2 = max(abs(y1(:  , 1)));
       max1 = find(abs(y1)== max2);
       shift = min(lags(max1));  
       shifts(1 , i) = shift;

       if (shift >= maxshift)
            shift = maxshift;
       elseif (shift <= -maxshift)
           shift = -maxshift;
       end


       if (shift > 0)
           erp( shift:240 , i) = erp( 1:(240-shift+1) , i);
           erp( 1:shift-1 , i) = 0;
       elseif (shift < 0)
           erp( 1:(240+shift+1) , i) = erp( -shift:240 , i);
           erp( 240+shift+2:end , i) = 0;
       end
    end
    if(mod(round , 10 ) == 0)
        plot(t , meansig);
        hold on;  
    end
    
end
final_mean = mean(ERP_EEG(: , 1:2500) , 2);
plot(t , final_mean , 'b--o');
hold on; 

xlabel("t(ms)");
ylabel("ERP response");
legend("without algorithm" , "10 iteration" , "20 iteration" , "30 iteration" , "40 iteration" , "50 iteration" , "60 iteration" , "70 iteration" , "80 iteration" , "90 iteration" , "100 iteration" , "mean of 2500");

a = mean(xcorr(final_mean , meansig));
b = mean(xcorr(final_mean , mean(ERP_EEG(: , 1:200) , 2)));


%% Q2
clc;
clear;
signal8 = zeros(5 , 128 , 3840);
load("SSVEP_8Hz_Trial1_SUBJ2.Mat");
signal8(1 , : , :) = EEGSignal;
load("SSVEP_8Hz_Trial2_SUBJ2.Mat");
signal8(2 , : , :) = EEGSignal;
load("SSVEP_8Hz_Trial3_SUBJ2.Mat");
signal8(3 , : , :) = EEGSignal;
load("SSVEP_8Hz_Trial4_SUBJ2.Mat");
signal8(4 , : , :) = EEGSignal;
load("SSVEP_8Hz_Trial5_SUBJ2.Mat");
signal8(5 , : , :) = EEGSignal;

signal14 = zeros(5 , 128 , 3840);
load("SSVEP_14Hz_Trial1_SUBJ2.Mat");
signal14(1 , : , :) = EEGSignal;
load("SSVEP_14Hz_Trial2_SUBJ2.Mat");
signal14(2 , : , :) = EEGSignal;
load("SSVEP_14Hz_Trial3_SUBJ2.Mat");
signal14(3 , : , :) = EEGSignal;
load("SSVEP_14Hz_Trial4_SUBJ2.Mat");
signal14(4 , : , :) = EEGSignal;
load("SSVEP_14Hz_Trial5_SUBJ2.Mat");
signal14(5 , : , :) = EEGSignal;

signal28 = zeros(5 , 128 , 3840);
load("SSVEP_28Hz_Trial1_SUBJ2.Mat");
signal28(1 , : , :) = EEGSignal;
load("SSVEP_28Hz_Trial2_SUBJ2.Mat");
signal28(2 , : , :) = EEGSignal;
load("SSVEP_28Hz_Trial3_SUBJ2.Mat");
signal28(3 , : , :) = EEGSignal;
load("SSVEP_28Hz_Trial4_SUBJ2.Mat");
signal28(4 , : , :) = EEGSignal;
load("SSVEP_28Hz_Trial5_SUBJ2.Mat");
signal28(5 , : , :) = EEGSignal;


%% Q2 alef

fs = 256;
t = 1/fs:1/fs:15;

[b,a]=butter(10,[4,50]/(fs/2),'bandpass');
y = filter(b,a,EEGSignal(10 , :));
for i = 1:5
    for j = 1:128
        signal8(i , j , :) = filter(b,a,signal8(i , j , :));
        signal14(i , j , :) = filter(b,a,signal14(i , j , :));
        signal28(i , j , :) = filter(b,a,signal28(i , j , :));
    end
end


%% Q2 be 
len = 3584;
signal_8 = zeros(5 , 128 , len);
signal_14 = zeros(5 , 128 , len);
signal_28 = zeros(5 , 128 , len);
for i = 1:5
    for j = 1:128
        signal_8(i , j , :) = signal8(i , j , 257:end);
        signal_14(i , j , :) = signal14(i , j , 257:end);
        signal_28(i , j , :) = signal28(i , j , 257:end);
    end
end


%% Q2 je
m_signal = zeros(128 , len);

msignal_8 = sum(signal_8(: , : , :) , 1);
msignal_14 = sum(signal_14(: , : , :) , 1);
msignal_28 = sum(signal_28(: , : , :) , 1);

m_signal(: , :) = msignal_8;

figure(1);
for i = 1:128
%     subplot(20 , 1 , i);
    x = m_signal(i , :);
%     pwelch(x);
    pxx = pwelch(x);
    f = 0:1/2:256;
    plot(f , (pxx));
    xlabel('Frequency (Hz)');
    ylabel('PSD (dB/Hz)');
    title("SSVEP 8 Hz");
    hold on
    
end

%%
m_signal(: , :) = msignal_14;

figure(1);
for i = 1:128
    x = m_signal(i , :);
    pxx = pwelch(x);
    f = 0:1/2:256;
    plot(f , (pxx));
    xlabel('Frequency (Hz)');
    ylabel('PSD (dB/Hz)');
    title("SSVEP 14 Hz");
    hold on
end

%%
m_signal(: , :) = msignal_28;

figure(1);
for i = 1:10
    x = m_signal(i , :);
    pxx = pwelch(x);
    f = 0:1/2:256;
    plot(f , (pxx));
    xlabel('Frequency (Hz)');
    ylabel('PSD (dB/Hz)');
    title("SSVEP 28 Hz");
    hold on 
end
